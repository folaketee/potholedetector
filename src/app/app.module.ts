import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { PotholeLogicPage } from '../pages/pothole-logic/pothole-logic';
import { ViewTripsPage } from '../pages/view-trips/view-trips';
import { AboutPage } from '../pages/about/about';
import { PlanTripsPage } from '../pages/plan-trips/plan-trips';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DeviceMotion } from '@ionic-native/device-motion';
import { GoogleMaps } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { PotholeProvider } from '../providers/pothole/pothole';
import {HttpModule} from "@angular/http";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    PotholeLogicPage,
    ViewTripsPage,
    AboutPage,
    PlanTripsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    PotholeLogicPage,
    ViewTripsPage,
    AboutPage,
    PlanTripsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DeviceMotion,
    GoogleMaps,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PotholeProvider
  ]
})
export class AppModule {}
