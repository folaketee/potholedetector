import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';
import {PotholeProvider} from '../../providers/pothole/pothole';
//import { DeviceMotion, DeviceMotionAccelerationData } from '@ionic-native/device-motion';
declare var google;
declare var window;


@IonicPage()
@Component({
  selector: 'page-view-trips',
  templateUrl: 'view-trips.html',
})
export class ViewTripsPage implements OnInit{

  map: any;
  trip_id = null;
  trips;
  treshold = 5.0000;
  msg;
  @ViewChild('map_canvas') mapElement:ElementRef;


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public geolocation : Geolocation,
              public potholeProvider: PotholeProvider) {
  }

  ionViewDidLoad() {
    this.loadMap ();
    this.createUser();
    if(!this.trip_id) {
        this.createTrip();
    }
  }
  ngOnInit(): void {

  }

  loadMap() {
      let locationOptions = {timeout:1000000, enableHighAccuracy: true};
      this.geolocation.watchPosition(locationOptions).subscribe((position) => {
          let latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          let mapOptions = {
              center:latlng,
              zoom:15,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
          let mark = {lat: position.coords.latitude, lng: position.coords.longitude};
          let marker = new google.maps.Marker({
              position: mark,
              map: this.map
          });
          /**this.deviceMotion.watchAcceleration().subscribe((acceleration: DeviceMotionAccelerationData) => {
              if (acceleration.z > this.treshold) {
                  this.msg = 'POTHOLE DETECTED';
                  alert('POTHOLE DETECTED');
                  this.addPothole(acceleration, position.coords.longitude, position.coords.latitude);
              }
          })**/
      }, (err) => {
          if(err) {
              console.log(JSON.stringify(err.message));
          }
      })
  }

  createUser() {
      this.potholeProvider.createUser().then((response) => {
          if(response) {
              this.potholeProvider.getUser().then((user) => {
                  this.trips = user.data.trip;
              })
          }
      })
  }

  createTrip() {
      this.potholeProvider.createTrip().then((response) => {
          this.trip_id = response.data.id;
      })
  }

  addPothole(location, longitude, latitude) {
      let newPothole = {
          'location':location,
          'lattitude':latitude,
          "longitude":longitude
      };
      this.potholeProvider.addPothole(newPothole,this.trip_id).then((response) => {

      })
  }
}
