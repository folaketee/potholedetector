import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PotholeLogicPage } from '../pothole-logic/pothole-logic';
import { ViewTripsPage } from '../view-trips/view-trips';
import { AboutPage } from '../about/about';
import {PlanTripsPage} from "../plan-trips/plan-trips";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  navigateToPotholeLogic() {
    this.navCtrl.push(PotholeLogicPage);
  }
  navigateToViewTrips() {
    this.navCtrl.push(ViewTripsPage);
  }
  navigateToAbout() {
    this.navCtrl.push(AboutPage);
  }
  navigateToPlanTrips() {
    this.navCtrl.push(PlanTripsPage);
  }
}
