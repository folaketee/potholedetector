import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlanTripsPage } from './plan-trips';

@NgModule({
  declarations: [
    PlanTripsPage,
  ],
  imports: [
    IonicPageModule.forChild(PlanTripsPage),
  ],
})
export class PlanTripsPageModule {}
