import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PotholeLogicPage } from './pothole-logic';

@NgModule({
  declarations: [
    PotholeLogicPage,
  ],
  imports: [
    IonicPageModule.forChild(PotholeLogicPage),
  ],
})
export class PotholeLogicPageModule {}
