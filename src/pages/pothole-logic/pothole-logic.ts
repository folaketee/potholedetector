import {Component, OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DeviceMotion, DeviceMotionAccelerationData } from '@ionic-native/device-motion';
import { Geolocation } from '@ionic-native/geolocation'

@IonicPage()
@Component({
  selector: 'page-pothole-logic',
  templateUrl: 'pothole-logic.html',
})
export class PotholeLogicPage implements OnInit{
    subscription;
    loc;
    acc;
    treshold = 5.0000;
    msg = 'NO POTHOLE YET';

  constructor(public navCtrl: NavController, public navParams: NavParams, private deviceMotion: DeviceMotion, private geolocation: Geolocation) {
      this.deviceMotion.watchAcceleration().subscribe((acceleration: DeviceMotionAccelerationData) => {
          this.acc = acceleration;
          if (this.acc.z > this.treshold) {
              this.msg = 'POTHOLE DETECTED';
          }
      this.watchPosition();
  })}



  ngOnInit() {
      this.getAcceleration();
  }

  getAcceleration() {
      this.deviceMotion.getCurrentAcceleration().then(
          (acceleration: DeviceMotionAccelerationData) =>// alert(JSON.stringify(acceleration)),
              (error: any) => console.log(error)
      );
      this.geolocation.getCurrentPosition().then((resp) => {
          // resp.coords.latitude
          // resp.coords.longitude
      }).catch((error) => {
          console.log('Error getting location', error);
      });
  }
    watchPosition () {
     // alert('hi');
         this.geolocation.watchPosition()
            .subscribe(position => {
                if(position) {
                     this.loc = position;
                }

                alert(JSON.stringify(position));
            });

    }

        // watchAcceleration() {
        //     var subscription = this.deviceMotion.watchAcceleration().subscribe((acceleration: DeviceMotionAccelerationData) => {
        //         console.log(acceleration);
        //     });
        // }


  ionViewDidLoad() {
    console.log('ionViewDidLoad PotholeLogicPage');
  }

}
